# i n c l u d e   " s t a c k _ d a t a . h "  
 u s i n g   n a m e s p a c e   s t d ;  
  
 / /   x 8 6  
 m a p < u i n t 3 2 _ t ,   F u n c t i o n D e t a i l s >   f u n c S t r u c t ;  
  
 v o i d   _ _ d e c l s p e c ( n a k e d )   D e t o u r F u n c t i o n R e a d ( )  
 {  
 	 / /   s t a t i c   v a l u e s   t o   r e a d   f r o m   t h e   f u n c t i o n  
 	 s t a t i c   u i n t 3 2 _ t   t m p _ r e g i s t e r 	 	 =   0 ;  
 	 s t a t i c   u i n t 3 2 _ t   t m p _ r e g i s t e r 2 	 	 =   0 ;  
 	 s t a t i c   u i n t 3 2 _ t   t m p _ r e g i s t e r 3 	 	 =   0 ;  
 	 s t a t i c   u i n t 3 2 _ t   t m p _ r e g i s t e r 4 	 	 =   0 ;  
  
 	 s t a t i c   u i n t 8 _ t   f u n c t i o n _ U I D 	 	 	 =   0 ;  
  
 	 s t a t i c   u i n t 8 _ t *   o v e r w r i t t e n _ b y t e s 	 =   N U L L ;  
  
 	 s t a t i c   R E G I S T R Y _ T Y P E   f r e e _ r e g 	 	 =   R E G I S T R Y _ T Y P E : : N O N E ;  
 	 s t a t i c   R E G I S T R Y _ T Y P E   r e g 	 	 	 =   R E G I S T R Y _ T Y P E : : N O N E ;  
  
 	 s t a t i c   i n t 3 2 _ t   o f f 	 	 	 	 	 =   0 ;  
  
 	 s t a t i c   u i n t 3 2 _ t   o b j e c t _ a d d r 	 	 	 =   0 ;  
  
 	 s t a t i c   u i n t 3 2 _ t   b y t e _ s i z e 	 	 	 =   0 ;  
 	 s t a t i c   u i n t 3 2 _ t 	 r e t _ a d d r 	 	 	 =   0 ;  
  
 	 _ _ a s m   m o v   t m p _ r e g i s t e r 2 ,   e b p  
 	 _ _ a s m   m o v   t m p _ r e g i s t e r 3 ,   e d x  
 	 _ _ a s m   m o v   t m p _ r e g i s t e r 4 ,   e s p  
  
 	 / /   h e r e   i s   a   f a n c y   b i t   o f   a s s e m b l y ,   w e   a r e   d o i n g   t h e   f o l l o w i n g :  
 	 / /   p o p   t h e   f i r s t   v a l u e   o n   t h e   s t a c k :   t h i s   i s   t h e   f u n c t i o n   I D   t h a t   w e   p u s h  
 	 / /   . . .   i n   t h e   f u n c t i o n   b e l o w .   w e   t h e n   u s e   t h i s   t o   r e t r i e v e   o u r   p a r a m e t e r s  
  
 	 / /   t m p   r e g i s t e r   s t o r e s   t h e   v a l u e   o f   t h e   E A X   r e g i s t e r   a n d   r e s t o r e s   i t  
 	 / /   . . .   a f t e r   r e a d i n g   f r o m   t h e   s t a c k .   ' p u s h a d '   i s   u s e d   t o   m a k e   s u r e   w e   a r e  
 	 / /   . . .   d e a l i n g   w i t h   c l e a n   r e g i s t e r s  
 	 _ _ a s m  
 	 {  
 	 	 m o v   t m p _ r e g i s t e r ,   e a x  
 	 	 p o p   e a x  
 	 	 p u s h a d  
  
 	 	 m o v   f u n c t i o n _ U I D ,   a l  
 	 	 m o v   e a x ,   t m p _ r e g i s t e r  
 	 }  
  
 	 / /   d e b u g   m e s s a g e  
 	 p r i n t f ( " F o u n d   % 0 2 X   o n   s t a c k \ n " ,   f u n c t i o n _ U I D ) ;  
  
 	 / /   r e a d   d a t a   f r o m   t h e   m a p  
 	 o v e r w r i t t e n _ b y t e s 	 	 =   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . b y t e s _ b e f o r e ;  
 	 f r e e _ r e g 	 	 	 	 =   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . f r e e _ r e g ;  
 	 r e g 	 	 	 	 	 	 =   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . r e g ;  
 	 o f f 	 	 	 	 	 	 =   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . o f f s e t ;  
 	 b y t e _ s i z e 	 	 	 	 =   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . b y t e _ s i z e   +   5 ;  
 	 r e t _ a d d r 	 	 	 	 =   b y t e _ s i z e   +   f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . h o o k _ a d d r e s s ;  
  
 	 / /   h e r e   w e   r e a d   a n d   d e c i d e   w h a t   t o   d o   b a s e d   o n   t h e   r e g i s t e r   p a r a m e t e r s  
 	 / /   I   k n o w   t h a t   s w i t c h e s   a r e   f a s t e r   f o r   s u c h   u s e s ,   b u t   i n   t h e   c o n t e x t  
 	 / /   . . .   o f   a   s t a t i c   f u n c t i o n ,   t h e y   ' m i g h t '   c a u s e   u n d e f i n e d   b e h a v i o u r  
 	 i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E A X )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e a x ,   e b p  
 	 	 	 	 a d d   e a x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e a x  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e a x ,   e s p  
 	 	 	 	 a d d   e a x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e a x  
 	 	 	 }  
 	 	 }  
 	 }  
 	 e l s e   i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E B X )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e b x ,   e b p  
 	 	 	 	 a d d   e b x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e b x  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e b x ,   e s p  
 	 	 	 	 a d d   e b x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e b x  
 	 	 	 }  
 	 	 }  
 	 }  
 	 e l s e   i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E C X )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e c x ,   e b p  
 	 	 	 	 a d d   e c x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e c x  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e c x ,   e s p  
 	 	 	 	 a d d   e c x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e c x  
 	 	 	 }  
 	 	 }  
 	 }  
 	 e l s e   i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E D X )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e d x ,   e b p  
 	 	 	 	 a d d   e d x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e d x  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e d x ,   e s p  
 	 	 	 	 a d d   e d x ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e d x  
 	 	 	 }  
 	 	 }  
 	 }  
 	 e l s e   i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E S I )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e s i ,   e b p  
 	 	 	 	 a d d   e s i ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e s i  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e s i ,   e s p  
 	 	 	 	 a d d   e s i ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e s i  
 	 	 	 }  
 	 	 }  
 	 }  
 	 e l s e   i f   ( f r e e _ r e g   = =   R E G I S T R Y _ T Y P E : : E D I )  
 	 {  
 	 	 i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E B P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e d i ,   e b p  
 	 	 	 	 a d d   e d i ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e d i  
 	 	 	 }  
 	 	 }  
 	 	 e l s e   i f   ( r e g   = =   R E G I S T R Y _ T Y P E : : E S P )  
 	 	 {  
 	 	 	 _ _ a s m  
 	 	 	 {  
 	 	 	 	 m o v   e d i ,   e s p  
 	 	 	 	 a d d   e d i ,   o f f  
  
 	 	 	 	 m o v   o b j e c t _ a d d r ,   e d i  
 	 	 	 }  
 	 	 }  
 	 }  
  
 	 / /   p r o c e s s   t h e   c a l l b a c k   w i t h   t h e   r e a d   d a t a  
 	 f u n c S t r u c t . a t ( f u n c t i o n _ U I D ) . c a l l b a c k ( f u n c t i o n _ U I D ,   o b j e c t _ a d d r ) ;  
  
 	 / /   p o p   a l l   t h e   r e g i s t e r s   f r o m   t h e   s t a c k  
 	 _ _ a s m   p o p a d  
  
 	 / /   d e b u g   m e s s a g e  
 	 p r i n t f ( " P o p p e d   e v e r y   r e g i s t e r   f r o m   t h e   s t a c k .   E x e c u t i n g   p a t c h i n g   s h e l l c o d e . \ n " ) ;  
 	  
 	 / /   e x e c u t e   t h e   s h e l l c o d e   t h a t   r e s t o r e s   t h e   f u n c t i o n   s t a t e  
 	 p r i n t f ( " J u m p i n g   t o   o v e r w r i t t e n   b y t e s :   % x \ n " ,   ( u i n t 3 2 _ t ) o v e r w r i t t e n _ b y t e s ) ;  
  
 	 _ _ a s m   m o v   e b p ,   t m p _ r e g i s t e r 2  
 	 _ _ a s m   m o v   e d x ,   t m p _ r e g i s t e r 3  
 	 _ _ a s m   m o v   e s p ,   t m p _ r e g i s t e r 4  
 	 ( ( v o i d ( * ) ( ) ) o v e r w r i t t e n _ b y t e s ) ( ) ;  
 }  
  
 v o i d   M a k e J u m p W i t h U I D ( D W O R D   a d d r e s s ,   c o n s t   s i z e _ t   s i z e ,   R E G I S T R Y _ T Y P E   f r e e _ r e g ,   R E G I S T R Y _ T Y P E   r e g ,   i n t 3 2 _ t   o f f s e t ,   f u n c t i o n < v o i d ( u i n t 8 _ t ,   u i n t 3 2 _ t ) >   c a l l b a c k )  
 {  
 	 / /   d u m m y   v a l u e  
 	 D W O R D   o l d p ;  
  
 	 / /   u n p r o t e c t   t h e   m e m o r y   s e g m e n t  
 	 V i r t u a l P r o t e c t ( ( v o i d * ) a d d r e s s ,   s i z e ,   P A G E _ E X E C U T E _ R E A D W R I T E ,   & o l d p ) ;  
  
 	 / /   c a l c u l a t e   t h e   I D   o f   t h i s   h o o k ' s   t a r g e t  
 	 s i z e _ t   f u n c t i o n _ I D   =   f u n c S t r u c t . s i z e ( ) ;  
  
 	 / /   c r e a t e   a n   e x e c u t a b l e   m e m o r y   b u f f e r   t o   n o t   g e t   D E P ' d  
 	 u i n t 8 _ t *   b y t e s _ o v w r   =   r e i n t e r p r e t _ c a s t < u i n t 8 _ t * > ( V i r t u a l A l l o c ( 0 ,   s i z e ,   M E M _ C O M M I T ,   P A G E _ E X E C U T E _ R E A D W R I T E ) ) ;  
 	  
 	 / /   i t ' s   s t i l l   a   n o r m a l   c h u n k   o f   m e m o r y   s o   w e   c a n   u s e   m e m c p y  
 	 m e m c p y ( b y t e s _ o v w r ,   ( v o i d * ) a d d r e s s ,   s i z e ) ;  
  
 	 / /   m a k e   a   j m p   x x x x x   i n s t r u c t i o n  
  
 	 / /   p u s h   [ B Y T E   V A L U E ]  
 	 * ( u n s i g n e d   c h a r * ) a d d r e s s   =   0 x 6 A ;  
 	  
 	 / /   p u s h   t h e   f u n c t i o n   I D   t o   t h e   s t a c k ,   m a k e   s u r e   i t ' s   o n l y   o n e   b y t e  
 	 * ( u n s i g n e d   c h a r * ) ( a d d r e s s   +   1 )   =   f u n c t i o n _ I D   &   0 x F F ;  
  
 	 / /   i n i t i a l i z e   a   n e a r - j u m p  
 	 * ( u n s i g n e d   c h a r * ) ( a d d r e s s   +   2 )   =   0 x E 9 ;  
  
 	 / /   c a l c u l a t e   t h e   n e a r - j m p   c o n t e x t   d i s p l a c e m e n t  
 	 * ( u n s i g n e d   i n t * ) ( a d d r e s s   +   3 )   =   ( i n t ) D e t o u r F u n c t i o n R e a d   -   ( a d d r e s s   +   7 ) ;  
  
 	 / /   p a d   t h e   r e s t  
 	 i f   ( s i z e   >   5 )   m e m s e t ( ( c h a r * ) a d d r e s s   +   7 ,   0 ,   s i z e   -   7 ) ;  
  
 	 / /   r e s e t   t h e   v i r t u a l   m e m o r y   a c c e s s  
 	 V i r t u a l P r o t e c t ( ( v o i d * ) a d d r e s s ,   s i z e ,   o l d p ,   & o l d p ) ;  
  
 	 / /   n e w   s t r u c t u r e   t o   h o l d   t h e   d e t a i l s   o f   o u r   t a r g e t  
 	 F u n c t i o n D e t a i l s   n e w S t r u c t ;  
  
 	 / /   s e t   t h e   a d d r e s s   t h a t   w e   a r e   h o o k i n g   a t  
 	 n e w S t r u c t . h o o k _ a d d r e s s   =   a d d r e s s ;  
  
 	 / /   . . .   a n d   t h e   b y t e s   w e   a r e   r e p l a c i n g  
 	 n e w S t r u c t . b y t e _ s i z e   =   s i z e ;  
  
 	 / /   a l l o c a t e   t h e   n e w   b u f f e r  
 	 n e w S t r u c t . b y t e s _ b e f o r e   =   r e i n t e r p r e t _ c a s t < u i n t 8 _ t * > ( V i r t u a l A l l o c ( 0 ,   s i z e   +   5 ,   M E M _ C O M M I T ,   P A G E _ E X E C U T E _ R E A D W R I T E ) ) ;  
 	  
 	 / /   c o p y   t h e   p r e v i o u s   b y t e s   o f   m e m o r y   h e r e  
 	 m e m c p y ( n e w S t r u c t . b y t e s _ b e f o r e ,   b y t e s _ o v w r ,   s i z e ) ;  
  
 	 / /   I S S U E :   u s i n g   e i t h e r   a   n e a r   j u m p   o r   a n   a b s o l u t e   j u m p   c r a s h e s  
 	 / /   m a y b e   u s e l e s s   s t u f f   i s   s t u c k   o n   t h e   s t a c k   o r   t h e   r e t u r n   a d d r e s s   i s   w r o n g  
 	 n e w S t r u c t . b y t e s _ b e f o r e [ s i z e ] 	 	 =   0 x E 9 ;  
  
 	 / /   a s s i g n   t h e   t a r g e t   o f   t h e   j u m p  
 	 / /   h e r e   w e   d o   a   R E L A T I V E   j u m p ,   x 8 6   j u m p   d i r e c t i v e s  
 	 / /   . . .   a r e   a l l   r e l a t i v e   b e c a u s e   t h e y   a r e   c o s t   e f f i c i e n t   f o r   t h e   C P U  
 	 p r i n t f ( " A d d r e s s   o f   b y t e s _ b e f o r e :   % x \ n " ,   ( u i n t 3 2 _ t ) n e w S t r u c t . b y t e s _ b e f o r e ) ;  
 	 * ( u n s i g n e d   i n t * ) ( n e w S t r u c t . b y t e s _ b e f o r e   +   s i z e   +   1 )   =   ( i n t ) ( a d d r e s s   +   s i z e )   -   ( i n t ) ( n e w S t r u c t . b y t e s _ b e f o r e   +   s i z e ) ;  
  
  
 	 / /   r e m e m b e r   w h a t   t y p e s   o f   r e g i s t e r s   t o   u s e   i n   t h e   h o o k  
 	 n e w S t r u c t . f r e e _ r e g   =   f r e e _ r e g ;  
 	 n e w S t r u c t . r e g   =   r e g ;  
 	  
 	 / /   r e m e m b e r   t h e   o f f s e t   w h e r e   w e   r e a d   f r o m  
 	 n e w S t r u c t . o f f s e t   =   o f f s e t ;  
  
 	 / /   c a l l b a c k   f u n c t i o n   t o   p r o c e s s   t h e   r e a d   d a t a  
 	 n e w S t r u c t . c a l l b a c k   =   c a l l b a c k ;  
  
 	 / /   f i n a l l y   a s s i g n   t h e   s t r u c t u r e   t o   t h e   m a p  
 	 f u n c S t r u c t [ f u n c t i o n _ I D ]   =   n e w S t r u c t ;  
  
 	 / /   R e l e a s e   t h e   m e m o r y  
 	 V i r t u a l F r e e ( b y t e s _ o v w r ,   0 ,   M E M _ R E L E A S E ) ;  
 }  
  
 F u n c t i o n D e t a i l s &   G e t D e t a i l s ( u i n t 8 _ t   i d )  
 {  
 	 r e t u r n   f u n c S t r u c t [ i d ] ;  
 } 