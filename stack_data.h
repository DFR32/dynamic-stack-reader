#pragma once
#include <cstdint>
#include <cstdlib>
#include <map>
#include <algorithm>
#include <functional>
#include <Windows.h>

enum REGISTRY_TYPE
{
	EAX,
	EBX,
	ECX,
	EDX,
	EBP,
	ESP,
	ESI,
	EDI,
	NONE
};

struct FunctionDetails
{
	uint32_t hook_address = 0;
	uint32_t byte_size = 0;
	uint8_t* bytes_before = NULL;

	REGISTRY_TYPE free_reg = REGISTRY_TYPE::NONE;
	REGISTRY_TYPE reg = REGISTRY_TYPE::NONE;

	int32_t offset = 0;

	std::function<void(uint8_t, uint32_t)> callback;
};


void MakeJumpWithUID(DWORD address, const size_t size, REGISTRY_TYPE free_reg, REGISTRY_TYPE reg, int32_t offset, std::function<void(uint8_t, uint32_t)> callback);
FunctionDetails& GetDetails(uint8_t id);