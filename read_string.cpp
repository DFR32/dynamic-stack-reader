#include <windows.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include "stack_data.h"
using namespace std;

typedef void(__cdecl* InitFunction_t)();
InitFunction_t InitFunction;

const char dll_name[] = "dll_test.dll";
DWORD dll_address = 0;

char buf[50] = { '\0' };

void __stdcall GotString(uint8_t function_id, uint32_t ptr)
{
	FunctionDetails& func_details = GetDetails(function_id);

	memcpy(buf, (char*)ptr, 48);


	printf("%s\n", buf);
}

int main()
{
	HANDLE hInst = LoadLibraryA(dll_name);

	// check if the dll loaded succesfully
	if (hInst == NULL)
	{
		// print an error otherwise
		printf("Error: %d\n", GetLastError());
	}

	// get the module address of the dll
	dll_address = (DWORD)GetModuleHandleA(dll_name);

	printf("Address is: %x\n", dll_address);

	InitFunction = (InitFunction_t)(dll_address + 0x1000);

	MakeJumpWithUID(dll_address + 0x1049, 7, REGISTRY_TYPE::EDX, REGISTRY_TYPE::EBP, -56, GotString);

	// call the function
	InitFunction();
	return 0;
}